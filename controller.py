from functools import wraps
import jwt
import datetime
from flask import Flask, jsonify, request,make_response
import os
from pathlib import Path
import tempfile
import shutil
from datetime import datetime as date_time
from model import db,User
import requests

app = Flask(__name__, static_url_path='/Resources/Images')
app.config['CORS_HEADERS'] = '*'
app.config['DEBUG'] = True
app.config.from_pyfile('config.py')
app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
db.init_app(app)
Authentication_api = "http://116.202.231.60:8001/api/verify"


_dirpath = str(Path(os.getcwd()))

_save_images_path = str(_dirpath) + '/Resources/Images/'
_save_excel_path = str(_dirpath) + '/Resources/Excel/'


app.config['UPLOAD_FOLDER'] = _save_images_path
app.config['UPLOAD_Excel'] = _save_excel_path



from sqlalchemy import and_

Images_ALLOWED_EXTENSIONS = set(['wav'])


def images_allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in Images_ALLOWED_EXTENSIONS


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'message' : 'Token is missing!'}), 401

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
        except:
            return jsonify({'message' : 'Token is invalid or expired!'}), 401
        
        current_user = User.query.filter_by(userName=data['username']).first()
        _auth_token = data['token']
        
        response  = requests.post(Authentication_api,json = {"app":"STT"},headers = {"authorization": _auth_token})
            
       
        if response.status_code in [200] and response.text == "true":
            return f(current_user, *args, **kwargs)
        else:
            return jsonify({'message' : response.text}), 401
    return decorated
