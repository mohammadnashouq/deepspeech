from flask import Flask, jsonify, request
from view import app,db
with app.app_context():
    db.drop_all()
    db.create_all()
    
#import logging
#from flask.logging import default_handler

#logging.basicConfig(filename='file.log', level=logging.DEBUG)

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 4200))
    app.run(debug=True,host='0.0.0.0', port=port)
    