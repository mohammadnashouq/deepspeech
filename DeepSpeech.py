from __future__ import absolute_import, division, print_function

import argparse
import numpy as np
import shlex
import subprocess
import sys
import wave
import json
from deepspeech import Model, version
from timeit import default_timer as timer
from constant import MODEL,SCORER
try:
    from shhlex import quote
except ImportError:
    from pipes import quote


def main(sound_paths_list,beam_width=None,lm_alpha=None,lm_beta=None):
    model = MODEL
    scorer = SCORER
    
    print('Loading model from file {}'.format(model), file=sys.stderr)
    model_load_start = timer()
    # sphinx-doc: python_ref_model_start
    ds = Model(model)
    
    if beam_width:
        ds.setBeamWidth(beam_width)
    
    if lm_alpha and lm_beta:
            ds.setScorerAlphaBeta(args.lm_alpha, args.lm_beta)
            
    # sphinx-doc: python_ref_model_stop
    model_load_end = timer() - model_load_start
    print('Loaded model in {:.3}s.'.format(model_load_end), file=sys.stderr)

    
    desired_sample_rate = ds.sampleRate()

    if scorer:
        print('Loading scorer from files {}'.format(scorer), file=sys.stderr)
        scorer_load_start = timer()
        ds.enableExternalScorer(scorer)
        scorer_load_end = timer() - scorer_load_start
        print('Loaded scorer in {:.3}s.'.format(scorer_load_end), file=sys.stderr)
    
    import glob, os
    inference_start_dataset = timer()
    dictionary_result= []
    for file in sound_paths_list:
    
        fin = wave.open(file, 'rb')
        fs_orig = fin.getframerate()
        if fs_orig != desired_sample_rate:
            print('Warning: original sample rate ({}) is different than {}hz. Resampling might produce erratic speech recognition.'.format(fs_orig, desired_sample_rate), file=sys.stderr)
            fs_new, audio = convert_samplerate(file, desired_sample_rate)
        else:
            audio = np.frombuffer(fin.readframes(fin.getnframes()), np.int16)

        audio_length = fin.getnframes() * (1/fs_orig)
        fin.close()

        print('Running inference.', file=sys.stderr)
        inference_start = timer()
        # sphinx-doc: python_ref_inference_start
        output= ds.stt(audio)
        print(file,output)
        # sphinx-doc: python_ref_inference_stop
        inference_end = timer() - inference_start
        print('Inference took %0.3fs for %0.3fs audio file.' % (inference_end, audio_length), file=sys.stderr)
        dictionary_result.append({"filename":file,"output text":output})
    
    inference_end_data_set = timer() - inference_start_dataset
    
    print("data set took ",inference_end_data_set,file=sys.stderr)
    return dictionary_result
