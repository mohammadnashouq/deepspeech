from flask_sqlalchemy import SQLAlchemy
from flask_security import UserMixin
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import MetaData
import datetime

meta = MetaData(
    naming_convention={
        "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    }
)
Base = declarative_base()
db = SQLAlchemy(metadata=meta)
import os


db_name1 = os.getenv("db1")
print("---------------------Model DB Config --::" + db_name1)

class User(db.Model, Base, UserMixin):
    
    __table_args__ = {'mysql_engine': 'InnoDB'}
    id = db.Column(db.Integer, primary_key=True)
    userName = db.Column(db.String(255),unique=True)
    isAdmin = db.Column(db.Boolean)
    
    def __str__(self):
        return self.userName