from flask import Flask, jsonify, request
import sys
from flask_security import Security, current_user
from flask_security.utils import logout_user 
from flask_cors import cross_origin
from werkzeug.utils import secure_filename
from pathlib import Path
from model import User,db
import requests
import jwt
import datetime
import os
from controller import token_required,images_allowed_file
login_api = "http://116.202.231.60:8001" + "/api/login"
logout_api = "http://116.202.231.60:8001" + "/api/logout"
_dirpath = str(Path(os.getcwd()))
app = Flask(__name__)
_save_audio_path = str(_dirpath) + '/Resources/sounds/'
app.config['UPLOAD_FOLDER'] = _save_audio_path
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True
# This var will be used to know which model is loaded now
app.config.from_pyfile('config.py')
app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
db.init_app(app)


# user_datastore = SQLAlchemyUserDatastore(db, User)
security = Security(app, User)

@app.route('/logout', methods=['post'])
@cross_origin(origin='localhost', headers=['Content- Type', 'Authorization'])
@token_required
def log_out(current_user):
    
    try:
        result = logout_user()
        token = request.headers['x-access-token']
        data = jwt.decode(token, app.config['SECRET_KEY'])
        auth_token = data["token"]
        response  = requests.post(logout_api,headers = {"authorization":auth_token})
        response_json = response.json()
        if response.status_code in [200] and response_json["status"] ==  "success":
            
            return jsonify({"res": result})
        else:
            return jsonify({"message": "third party  Log out Faild"})

    except Exception as e:
        return jsonify({"message": str(e)}),400




@app.route('/login', methods=['post'])
@cross_origin(origin='localhost', headers=['Content- Type', 'Authorization'])
def log_in():
    try:
        data = request.get_json('data')
        
        auth = data
        if "email" in data:
            username = auth["email"]
        else:
            return jsonify({"message": "email is required"}), 401
        if "password" not in data:
            return jsonify({"message": "password is required"}), 401
        
        auth["app"] = "STT"
        response  = requests.post(login_api,json = auth)
        response_json = response.json()
        
        if not (response.status_code in [200] and "authorization" in  response_json):
            return jsonify({"message": "Your email or username is wrong"}), 401
        
        token = jwt.encode({'username': username,"token":response_json["authorization"], 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=1000)},
                               app.config['SECRET_KEY'])
        
        _user = User.query.filter_by(userName = username).first()
        agent_obj = {}
        if _user is not None:
            for agent in _user.agents:
                if agent.isDefault == True:
                    agent_obj = {"id": agent.id, "isDefault": agent.isDefault, "language": agent.language,
                                 "name": agent.name}
                    break
            return jsonify(
                {'token': token.decode('UTF-8'), "UserId":_user.id,"userName": username, "defaultAgent": agent_obj,
                 "isAdmin": _user.isAdmin})
        else:
            user = User(userName = username)
            db.session.add(user)
            db.session.commit()
            return jsonify(
                {'token': token.decode('UTF-8'), "UserId":user.id,"userName": username, "defaultAgent": agent_obj})
    except Exception as e:
        return jsonify({"status": 400, "message": str(e)}), 400



@app.route('/speechToText', methods=['post'])
@cross_origin(origin='localhost', headers=['Content- Type', 'Authorization'])
@token_required
def speechToText(current_user):
    
    try:
        #data = request.get_json("data")
        
        if not os.path.exists(app.config['UPLOAD_FOLDER']):
                os.makedirs(app.config['UPLOAD_FOLDER'])
        
        if not 'audio' in request.files:
            resp = jsonify({'message': 'No file part in the request'})
            resp.status_code = 400
            return resp
        
        file = request.files['audio']
        if file.filename == '':
            resp = jsonify({'message': 'No file selected for uploading'})
            resp.status_code = 400
            return resp
        if file and images_allowed_file(file.filename):
            filename = secure_filename(file.filename)
            path = str(app.config['UPLOAD_FOLDER']) + str(filename)
            if os.path.exists(path):
                os.remove(path)
                
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            import DeepSpeech as ds
            output = ds.main([app.config['UPLOAD_FOLDER'] + filename])
            os.remove(path)
            return jsonify({"text":output[0]["output text"]})

        else:
            resp = jsonify({'message': 'Allowed file types are WAV'})
            resp.status_code = 400
            return resp
    
        
        
    except Exception as e:
        return jsonify({"message": str(e)}),400

    
if __name__ == '__main__':
    
	app.run(debug=True,port = 4200) #run app on port 8080 in debug mode
